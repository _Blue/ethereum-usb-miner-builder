if [[ -o interactive ]]; then

  if ! pgrep ethminer > /dev/null; then
    echo "Disabled powersaving stuff"
    setterm -blank 0 -powersave off
    echo "Start mining in 5 seconds"
    sleep 5 && ~/mine.sh
  else
    echo "Already mining. Not doing it again"
  fi
fi


