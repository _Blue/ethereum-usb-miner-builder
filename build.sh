#! /bin/bash

set -ue
set -o pipefail
shopt -s xpg_echo

release=bionic
WORKDIR=/tmp/img
GRUB_ENTRY='[BLUE] Etereum mining'
root="$WORKDIR/tmp"
img_size=4000000 # KB

# Create a DOS volume, partition it, format the partition and mount it
prepare_image()
{
  mkdir -p "$WORKDIR"

  dd if=/dev/zero of="$WORKDIR/image" bs=1024 count=$img_size iflag=fullblock status=progress

  (
  echo o # Create a new empty DOS partition table
  echo n # Add a new partition
  echo p # Primary partition
  echo 1 # Partition number
  echo   # First sector (Accept default: 1)
  echo   # Last sector (Accept default: varies)
  echo w # Write changes
  ) | fdisk "$WORKDIR/image"

  disk=$(losetup --partscan --show --find "$WORKDIR/image")

  mkfs.ext4 "$disk"p1

  mkdir -p "$root"
  partition="$disk"p1

  mount "$disk"p1 "$root"
}

# Unmount filesystem and loop devices previously mounted
umount_image()
{
  umount -R "$root"

  if $lo_image; then
    losetup -d /dev/loop0
  fi
}

# Run a command inside the chroot
run()
{
  if ! chroot "$root" $*; then
    echo "Error running: $*"
    return 1
  fi
}

write() #Content, dest
{
  echo "$1" > "$root/$2"
}

# Install the base system, mount /dev, /proc, ...
bootstrap()
{
  mkdir -p "$root"
  debootstrap "$release" "$root"

  write "###### Ubuntu Main Repos
  deb http://se.archive.ubuntu.com/ubuntu/ $release main restricted universe multiverse
  deb-src http://se.archive.ubuntu.com/ubuntu/ $release main restricted universe multiverse

  ###### Ubuntu Update Repos
  deb http://se.archive.ubuntu.com/ubuntu/ $release-security main restricted universe multiverse
  deb http://se.archive.ubuntu.com/ubuntu/ $release-updates main restricted universe multiverse
  deb-src http://se.archive.ubuntu.com/ubuntu/ $release-security main restricted universe multiverse
  deb-src http://se.archive.ubuntu.com/ubuntu/ $release-updates main restricted universe multiverse
  " /etc/apt/sources.list

  run apt-get update

  run mount -t devtmpfs dev /dev
  run mount -t tmpfs -o rw,nosuid,nodev,noexec  tmpfs /dev/shm
  run mount -t proc proc /proc
  run mount -t sysfs sysfs /sys


  echo 'DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold"\
    install linux-image-generic squashfs-tools rsync live-boot ifupdown zsh openssh-server vim' | chroot "$root" /bin/bash

  run passwd -d root
  run chsh -s /bin/zsh
}

# Build ethminer from source
build_miner()
{
  run apt-get install -y dkms linux-headers-generic # Force Trigger installation of nvidia kernel module
  run apt-get install -y --no-install-recommends g++ cmake make git opencl-headers mesa-common-dev nvidia-opencl-icd-384


  run systemctl disable lightdm.service

  echo '
  export HOME=/root
  cd /root
  git clone https://github.com/ethereum-mining/ethminer --recursive --depth=1
  cd ethminer
  mkdir build
  cd build
  cmake .. -DETHASHCUDA=OFF -DETHASHCL=1
  make install
  rm -rf /root/.hunter $HOME/ethminer' | chroot "$root" /bin/bash

  run apt-get autoremove -y
}

# Configure grub
setup_boot()
{
  # Uncomment if not using squashfs
  #uid=$(blkid -o value "$partition" | head -n 1)
  #write "UUID=$uid /               ext4    errors=remount-ro 0       1" /etc/fstab

  run grub-mkconfig -o "boot/grub/grub.cfg"
  run grub-install "$disk"
  run chown -R root:root /root
}

# Apply configuration files in './files' and user script
apply_config()
{
  rsync -rax files/* "$root"
  run locale-gen

  if [ -f extra-conf.sh ]; then
    chroot "$root" /bin/bash < extra-conf.sh
  fi
}

# Build squashfs
make_squash()
{
  run mkdir /live

  echo 'setopt nullglob
        rm -rf /var/cache/apt/archives/* /var/cuda-repo-9-1-local /usr/local/cuda/samples/*
        mksquashfs / /live/filesystem.squashfs -always-use-fragments -noappend -e /proc/* -e /tmp/* -e /sys/* -e /dev/* -e /live' | chroot "$root" /bin/zsh


  # We need two specific parameters for the network to work
  # - net.ifnames: Use predictable names for interfaces
  # - ip=frommedia: Initrd seems to overwrite /etc/network/interfaces, this values disables this behavior

  write "#!/bin/sh
  exec tail -n +3 \$0
  menuentry \"$GRUB_ENTRY\" {
  set root='(hd0,1)'
  linux /vmlinuz boot=live toram=filesystem.squashfs net.ifnames=0 ip=frommedia
  initrd /initrd.img
  }" /etc/grub.d/40_custom

  run chmod -x /etc/grub.d/10_linux # Disable default grub entries

  run grub-mkconfig -o "boot/grub/grub.cfg"
}


lo_image=false

if [ ${1:-''} == '-u' ]; then
  lo_image=true
  umount_image
  exit
elif [ ${1:-''} == '-d' ]; then
  prepare_image
  lo_image=true
elif [ $# -lt 3 ]; then
  echo "usage: $0 mountpoint device partition"
  exit 1
else
  root="$1"
  disk="$2"
  partition="$3"
fi

bootstrap
build_miner
apply_config
setup_boot
make_squash
run update-grub
umount_image
