# What am I looking at ?

This repository contains a bash script that builds a disk image that mines ethereum once booted

Usage:

```
./build.sh mnt dev volume

```

Example usage:

```
# To install the image on /dev/sdb

mkfs.ext4 /dev/sdb1
mkdir /mnt/usb-miner
mount /dev/sdb1 /mnt/usb-miner

./build.sh /mnt/usb-miner /dev/sdb /dev/sdb1
```

## Extra configuration


* `./build.sh` will execute `./extra-conf.sh` inside the chroot, if this file exists
* You may need to install specific drivers for your GPU to mine efficiently
* Once booted, the script `/root/mine.sh` is executed

Example script:

```
#! /bin/bash

exec ethminer -G -F http://my_miniing_pool/my_eth_address
```


## Tips

* `./build.sh -d` creates a disk image in `/tmp/img` which can be booted using qemu or Virtualbox, which makes debugging easier
* To create a virtualbox compatible disk from a raw image, use: `vboxmanage convertdd [raw_image] [destination] --format VDI`
* The root filesystem is a squashfs which is loaded at boot, which means that once built, no modification is persistent
* The point of using squashfs is to reduce IO so that the usb stick lasts as long as possible
* The script installs nvidia display driver by default. If you have a different gpu, you'll need to install the appropriate driver
* This script is running `debootstrap`, so it probably won't work on non-debian based distributions
* Once built, the disk image weighs around 3GB, and the squashfs around 1GB, so I recommend at least a 4GB USB stick, and 2GB of RAM

**Be careful when running this script !**
If you enter the wrong device or mountpoint, it can destroy your machine !
